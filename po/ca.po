# Catalan translations for pavucontrol package.
# Copyright (C) 2008 Red Hat, Inc.
# This file is distributed under the same license as the
#  pavucontrol package.
#
# Xavier Queralt Mateu <xqueralt@gmail.com>, 2008.
#
# This file is translated according to the glossary and style guide of
#   Softcatalà. If you plan to modify this file, please read first the page
#   of the Catalan translation team for the Fedora project at:
#   http://www.softcatala.org/projectes/fedora/
#   and contact the previous translator
#
# Aquest fitxer s'ha de traduir d'acord amb el recull de termes i la guia
#   d'estil de Softcatalà. Si voleu modificar aquest fitxer, llegiu si
#   us plau la pàgina de catalanització del projecte Fedora a:
#   http://www.softcatala.org/projectes/fedora/
#   i contacteu l'anterior traductor/a.
#
msgid ""
msgstr ""
"Project-Id-Version: pavucontrol\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-13 09:18+0200\n"
"PO-Revision-Date: 2009-09-12 21:04+0100\n"
"Last-Translator: Agustí Grau <fletxa@gmail.com>\n"
"Language-Team: Catalan <fedora@softcatala.net>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/pavucontrol.desktop.in.h:1 ../src/pavucontrol.cc:590
msgid "PulseAudio Volume Control"
msgstr "Control del volum del PulseAudio"

#: ../src/pavucontrol.desktop.in.h:2 ../src/pavucontrol.glade.h:31
msgid "Volume Control"
msgstr "Control del volum"

#: ../src/pavucontrol.desktop.in.h:3
msgid "Adjust the volume level"
msgstr "Ajusta el volum"

#: ../src/pavucontrol.desktop.in.h:4
msgid ""
"pavucontrol;Microphone;Volume;Fade;Balance;Headset;Audio;Mixer;Output;Input;"
msgstr ""

#: ../src/pavucontrol.glade.h:1
msgid "<b>left-front</b>"
msgstr "<b>esquerra-davant</b>"

#: ../src/pavucontrol.glade.h:3
#, fuzzy, no-c-format
#| msgid "<small>Min</small>"
msgid "<small>50%</small>"
msgstr "<small>Min</small>"

#: ../src/pavucontrol.glade.h:4
msgid "Card Name"
msgstr "Nom de la targeta"

#: ../src/pavucontrol.glade.h:5
msgid "<b>Profile:</b>"
msgstr "<b>Perfil:</b>"

#: ../src/pavucontrol.glade.h:6
#, fuzzy
#| msgid "Device"
msgid "Device Title"
msgstr "Dispositiu"

#: ../src/pavucontrol.glade.h:7
msgid "Mute audio"
msgstr "Silencia"

#: ../src/pavucontrol.glade.h:8
msgid "Lock channels together"
msgstr "Bloca els canals junts"

#: ../src/pavucontrol.glade.h:9
msgid "Set as fallback"
msgstr "Estableix com a predeterminat"

#: ../src/pavucontrol.glade.h:10
msgid "<b>Port:</b>"
msgstr "<b>Port:</b>"

#: ../src/pavucontrol.glade.h:11
msgid "PCM"
msgstr ""

#: ../src/pavucontrol.glade.h:12
msgid "AC3"
msgstr ""

#: ../src/pavucontrol.glade.h:13
msgid "DTS"
msgstr ""

#: ../src/pavucontrol.glade.h:14
msgid "EAC3"
msgstr ""

#: ../src/pavucontrol.glade.h:15
msgid "MPEG"
msgstr ""

#: ../src/pavucontrol.glade.h:16
msgid "AAC"
msgstr ""

#: ../src/pavucontrol.glade.h:17
msgid "<b>Latency offset:</b>"
msgstr ""

#: ../src/pavucontrol.glade.h:18
msgid "ms"
msgstr ""

#: ../src/pavucontrol.glade.h:19
msgid "Advanced"
msgstr ""

#: ../src/pavucontrol.glade.h:20
msgid "All Streams"
msgstr ""

#: ../src/pavucontrol.glade.h:21
msgid "Applications"
msgstr ""

#: ../src/pavucontrol.glade.h:22
msgid "Virtual Streams"
msgstr ""

#: ../src/pavucontrol.glade.h:23
#, fuzzy
#| msgid "_Output Devices"
msgid "All Output Devices"
msgstr "Dispositius de s_ortida"

#: ../src/pavucontrol.glade.h:24
#, fuzzy
#| msgid "_Output Devices"
msgid "Hardware Output Devices"
msgstr "Dispositius de s_ortida"

#: ../src/pavucontrol.glade.h:25
#, fuzzy
#| msgid "_Output Devices"
msgid "Virtual Output Devices"
msgstr "Dispositius de s_ortida"

#: ../src/pavucontrol.glade.h:26
#, fuzzy
#| msgid "_Input Devices"
msgid "All Input Devices"
msgstr "D_ispositius d'entrada"

#: ../src/pavucontrol.glade.h:27
msgid "All Except Monitors"
msgstr ""

#: ../src/pavucontrol.glade.h:28
#, fuzzy
#| msgid "_Input Devices"
msgid "Hardware Input Devices"
msgstr "D_ispositius d'entrada"

#: ../src/pavucontrol.glade.h:29
#, fuzzy
#| msgid "_Input Devices"
msgid "Virtual Input Devices"
msgstr "D_ispositius d'entrada"

#: ../src/pavucontrol.glade.h:30
msgid "Monitors"
msgstr ""

#: ../src/pavucontrol.glade.h:32
msgid "<i>No application is currently playing audio.</i>"
msgstr "<i>Cap aplicació està reproduïnt àudio actualment</i>"

#: ../src/pavucontrol.glade.h:33
msgid "<b>_Show:</b>"
msgstr "<b>Mo_stra:</b>"

#: ../src/pavucontrol.glade.h:34
msgid "_Playback"
msgstr "Re_producció"

#: ../src/pavucontrol.glade.h:35
msgid "<i>No application is currently recording audio.</i>"
msgstr "<i>Cap aplicació està enregistrant àudio actualment.</i>"

#: ../src/pavucontrol.glade.h:36
msgid "_Recording"
msgstr "En_registrament"

#: ../src/pavucontrol.glade.h:37
msgid "<i>No output devices available</i>"
msgstr "<i>No hi ha cap dispositiu de sortida disponible</i>"

#: ../src/pavucontrol.glade.h:38
msgid "<b>S_how:</b>"
msgstr "<b>_Mostra:</b>"

#: ../src/pavucontrol.glade.h:39
msgid "_Output Devices"
msgstr "Dispositius de s_ortida"

#: ../src/pavucontrol.glade.h:40
msgid "<i>No input devices available</i>"
msgstr "<i>No hi ha cap dispositiu d'entrada disponible</i>"

#: ../src/pavucontrol.glade.h:41
msgid "<b>Sho_w:</b>"
msgstr "<b>Mos_tra:</b>"

#: ../src/pavucontrol.glade.h:42
msgid "_Input Devices"
msgstr "D_ispositius d'entrada"

#: ../src/pavucontrol.glade.h:43
msgid "<i>No cards available for configuration</i>"
msgstr "<i>No hi han targetes disponibles per a configurar</i>"

#: ../src/pavucontrol.glade.h:44
msgid "Show volume meters"
msgstr ""

#: ../src/pavucontrol.glade.h:45
msgid "_Configuration"
msgstr "_Configuració"

#: ../src/pavucontrol.glade.h:46
msgid "<b>Rename device to:</b>"
msgstr ""

#: ../src/pavucontrol.glade.h:47
msgid "Stream Title"
msgstr "Títol del flux"

#: ../src/pavucontrol.glade.h:48
msgid "direction"
msgstr "direcció"

#: ../src/pavucontrol.cc:79
msgid "Card callback failure"
msgstr "Ha fallat la crida de retorn a la targeta"

#: ../src/pavucontrol.cc:102
msgid "Sink callback failure"
msgstr "Ha fallat la crida de retorn del conducte"

#: ../src/pavucontrol.cc:126
msgid "Source callback failure"
msgstr "Ha fallat la crida de retorn de l'origen"

#: ../src/pavucontrol.cc:145
msgid "Sink input callback failure"
msgstr "Ha fallat la crida de retorn del conducte d'entrada"

#: ../src/pavucontrol.cc:164
msgid "Source output callback failure"
msgstr "Ha fallat la crida de retorn de l'origen de la sortida"

#: ../src/pavucontrol.cc:194
msgid "Client callback failure"
msgstr "Ha fallat la crida de retorn del client"

#: ../src/pavucontrol.cc:210
msgid "Server info callback failure"
msgstr "Ha fallat la crida de retorn de la informació del servidor"

#: ../src/pavucontrol.cc:228 ../src/pavucontrol.cc:525
#, c-format
msgid "Failed to initialize stream_restore extension: %s"
msgstr "Ha fallat la inicialització de l'extensió stream_restore: %s"

#: ../src/pavucontrol.cc:246
msgid "pa_ext_stream_restore_read() failed"
msgstr "Ha fallat pa_ext_stream_restore_read()"

#: ../src/pavucontrol.cc:264 ../src/pavucontrol.cc:539
#, fuzzy, c-format
#| msgid "Failed to initialize stream_restore extension: %s"
msgid "Failed to initialize device restore extension: %s"
msgstr "Ha fallat la inicialització de l'extensió stream_restore: %s"

#: ../src/pavucontrol.cc:285
#, fuzzy
#| msgid "pa_ext_stream_restore_read() failed"
msgid "pa_ext_device_restore_read_sink_formats() failed"
msgstr "Ha fallat pa_ext_stream_restore_read()"

#: ../src/pavucontrol.cc:303 ../src/pavucontrol.cc:552
#, fuzzy, c-format
#| msgid "Failed to initialize stream_restore extension: %s"
msgid "Failed to initialize device manager extension: %s"
msgstr "Ha fallat la inicialització de l'extensió stream_restore: %s"

#: ../src/pavucontrol.cc:322
#, fuzzy
#| msgid "pa_ext_stream_restore_read() failed"
msgid "pa_ext_device_manager_read() failed"
msgstr "Ha fallat pa_ext_stream_restore_read()"

#: ../src/pavucontrol.cc:339
msgid "pa_context_get_sink_info_by_index() failed"
msgstr "Ha fallat pa_context_get_sink_info_by_index()"

#: ../src/pavucontrol.cc:352
msgid "pa_context_get_source_info_by_index() failed"
msgstr "Ha fallat pa_context_get_source_info_by_index()"

#: ../src/pavucontrol.cc:365 ../src/pavucontrol.cc:378
msgid "pa_context_get_sink_input_info() failed"
msgstr "Ha fallat pa_context_get_sink_input_info()"

#: ../src/pavucontrol.cc:391
msgid "pa_context_get_client_info() failed"
msgstr "Ha fallat pa_context_get_client_info()"

#: ../src/pavucontrol.cc:401 ../src/pavucontrol.cc:466
msgid "pa_context_get_server_info() failed"
msgstr "Ha fallat pa_context_get_server_info()"

#: ../src/pavucontrol.cc:414
msgid "pa_context_get_card_info_by_index() failed"
msgstr "Ha fallat pa_context_get_card_info_by_index()"

#: ../src/pavucontrol.cc:457
msgid "pa_context_subscribe() failed"
msgstr "Ha fallat pa_context_subscribe()"

#: ../src/pavucontrol.cc:473
msgid "pa_context_client_info_list() failed"
msgstr "Ha fallat pa_context_client_info_list()"

#: ../src/pavucontrol.cc:480
msgid "pa_context_get_card_info_list() failed"
msgstr "Ha fallat pa_context_get_card_info_list()"

#: ../src/pavucontrol.cc:487
msgid "pa_context_get_sink_info_list() failed"
msgstr "Ha fallat pa_context_get_sink_info_list()"

#: ../src/pavucontrol.cc:494
msgid "pa_context_get_source_info_list() failed"
msgstr "Ha fallat pa_context_get_source_info_list()"

#: ../src/pavucontrol.cc:501
msgid "pa_context_get_sink_input_info_list() failed"
msgstr "Ha fallat pa_context_get_sink_input_info_list()"

#: ../src/pavucontrol.cc:508
msgid "pa_context_get_source_output_info_list() failed"
msgstr "Ha fallat pa_context_get_source_output_info_list()"

#: ../src/pavucontrol.cc:567 ../src/pavucontrol.cc:618
#, fuzzy
#| msgid "Connection failed"
msgid "Connection failed, attempting reconnect"
msgstr "Ha fallat la connexió"

#: ../src/pavucontrol.cc:605
msgid ""
"Connection to PulseAudio failed. Automatic retry in 5s\n"
"\n"
"In this case this is likely because PULSE_SERVER in the Environment/X11 Root "
"Window Properties\n"
"or default-server in client.conf is misconfigured.\n"
"This situation can also arise when PulseAudio crashed and left stale details "
"in the X11 Root Window.\n"
"If this is the case, then PulseAudio should autospawn again, or if this is "
"not configured you should\n"
"run start-pulseaudio-x11 manually."
msgstr ""

#: ../src/cardwidget.cc:88
msgid "pa_context_set_card_profile_by_index() failed"
msgstr "Ha fallat pa_context_set_card_profile_by_index()"

#: ../src/channelwidget.cc:101
#, c-format
msgid "<small>%0.0f%% (%0.2f dB)</small>"
msgstr ""

#: ../src/channelwidget.cc:103
#, c-format
msgid "<small>%0.0f%% (-&#8734; dB)</small>"
msgstr ""

#: ../src/channelwidget.cc:106
#, c-format
msgid "%0.0f%%"
msgstr ""

#: ../src/channelwidget.cc:139
msgid "<small>Silence</small>"
msgstr "<small>Silenci</small>"

#: ../src/channelwidget.cc:139
msgid "<small>Min</small>"
msgstr "<small>Min</small>"

#: ../src/channelwidget.cc:141
#, fuzzy
#| msgid "<small>Min</small>"
msgid "<small>100% (0 dB)</small>"
msgstr "<small>Min</small>"

#: ../src/channelwidget.cc:145
msgid "<small><i>Base</i></small>"
msgstr "<small><i>Base</i></small>"

#: ../src/devicewidget.cc:59
msgid "Rename Device..."
msgstr ""

#: ../src/devicewidget.cc:163
#, fuzzy
#| msgid "pa_context_set_sink_port_by_index() failed"
msgid "pa_context_set_port_latency_offset() failed"
msgstr "Ha fallat pa_context_set_sink_port_by_index()"

#: ../src/devicewidget.cc:244
msgid "Sorry, but device renaming is not supported."
msgstr ""

#: ../src/devicewidget.cc:249
msgid ""
"You need to load module-device-manager in the PulseAudio server in order to "
"rename devices"
msgstr ""

#: ../src/devicewidget.cc:270
#, fuzzy
#| msgid "pa_ext_stream_restore_write() failed"
msgid "pa_ext_device_manager_write() failed"
msgstr "Ha fallat pa_ext_stream_restore_write()"

#: ../src/mainwindow.cc:171
#, c-format
msgid "Error reading config file %s: %s"
msgstr ""

#: ../src/mainwindow.cc:249
msgid "Error saving preferences"
msgstr ""

#: ../src/mainwindow.cc:257
#, c-format
msgid "Error writing config file %s"
msgstr ""

#: ../src/mainwindow.cc:321
msgid " (plugged in)"
msgstr ""

#: ../src/mainwindow.cc:325 ../src/mainwindow.cc:431
msgid " (unavailable)"
msgstr ""

#: ../src/mainwindow.cc:327 ../src/mainwindow.cc:428
msgid " (unplugged)"
msgstr ""

#: ../src/mainwindow.cc:563
msgid "Failed to read data from stream"
msgstr "Ha fallat la lectura del fluxe"

#: ../src/mainwindow.cc:607
msgid "Peak detect"
msgstr "Detecta els pics"

#: ../src/mainwindow.cc:608
msgid "Failed to create monitoring stream"
msgstr "Ha fallat la creació del fluxe de monitorització"

#: ../src/mainwindow.cc:623
msgid "Failed to connect monitoring stream"
msgstr "Ha fallat la connexió al fluxe de monitorització"

#: ../src/mainwindow.cc:759
msgid ""
"Ignoring sink-input due to it being designated as an event and thus handled "
"by the Event widget"
msgstr ""

#: ../src/mainwindow.cc:934
msgid "System Sounds"
msgstr "Sons del sistema"

#: ../src/mainwindow.cc:1280
msgid "Establishing connection to PulseAudio. Please wait..."
msgstr ""

#: ../src/rolewidget.cc:72
msgid "pa_ext_stream_restore_write() failed"
msgstr "Ha fallat pa_ext_stream_restore_write()"

#: ../src/sinkinputwidget.cc:35
msgid "on"
msgstr "a"

#: ../src/sinkinputwidget.cc:38
msgid "Terminate Playback"
msgstr "Finalitza la reproducció"

#: ../src/sinkinputwidget.cc:78
msgid "Unknown output"
msgstr "Sortida desconeguda"

#: ../src/sinkinputwidget.cc:87
msgid "pa_context_set_sink_input_volume() failed"
msgstr "Ha fallat pa_context_set_sink_input_volume()"

#: ../src/sinkinputwidget.cc:102
msgid "pa_context_set_sink_input_mute() failed"
msgstr "Ha fallat pa_context_set_sink_input_mute()"

#: ../src/sinkinputwidget.cc:112
msgid "pa_context_kill_sink_input() failed"
msgstr "Ha fallat pa_context_kill_sink_input()"

#: ../src/sinkwidget.cc:95
msgid "pa_context_set_sink_volume_by_index() failed"
msgstr "Ha fallat pa_context_set_sink_volume_by_index()"

#: ../src/sinkwidget.cc:110
msgid "Volume Control Feedback Sound"
msgstr "Control del volum de la realimentació del so"

#: ../src/sinkwidget.cc:127
msgid "pa_context_set_sink_mute_by_index() failed"
msgstr "Ha fallat pa_context_set_sink_mute_by_index()"

#: ../src/sinkwidget.cc:141
msgid "pa_context_set_default_sink() failed"
msgstr "Ha fallat pa_context_set_default_sink()"

#: ../src/sinkwidget.cc:161
msgid "pa_context_set_sink_port_by_index() failed"
msgstr "Ha fallat pa_context_set_sink_port_by_index()"

#: ../src/sinkwidget.cc:203
#, fuzzy
#| msgid "pa_ext_stream_restore_read() failed"
msgid "pa_ext_device_restore_save_sink_formats() failed"
msgstr "Ha fallat pa_ext_stream_restore_read()"

#: ../src/sourceoutputwidget.cc:35
msgid "from"
msgstr "des de"

#: ../src/sourceoutputwidget.cc:38
msgid "Terminate Recording"
msgstr "Termina l'enregistrament"

#: ../src/sourceoutputwidget.cc:83
msgid "Unknown input"
msgstr "Entrada desconeguda"

#: ../src/sourceoutputwidget.cc:93
#, fuzzy
#| msgid "pa_context_get_source_output_info_list() failed"
msgid "pa_context_set_source_output_volume() failed"
msgstr "Ha fallat pa_context_get_source_output_info_list()"

#: ../src/sourceoutputwidget.cc:108
#, fuzzy
#| msgid "pa_context_get_source_output_info_list() failed"
msgid "pa_context_set_source_output_mute() failed"
msgstr "Ha fallat pa_context_get_source_output_info_list()"

#: ../src/sourceoutputwidget.cc:119
msgid "pa_context_kill_source_output() failed"
msgstr "Ha fallat pa_context_kill_source_output()"

#: ../src/sourcewidget.cc:46
msgid "pa_context_set_source_volume_by_index() failed"
msgstr "Ha fallat pa_context_set_source_volume_by_index()"

#: ../src/sourcewidget.cc:61
msgid "pa_context_set_source_mute_by_index() failed"
msgstr "Ha fallat pa_context_set_source_mute_by_index()"

#: ../src/sourcewidget.cc:75
msgid "pa_context_set_default_source() failed"
msgstr "Ha fallat pa_context_set_default_source()"

#: ../src/sourcewidget.cc:97
msgid "pa_context_set_source_port_by_index() failed"
msgstr "Ha fallat pa_context_set_source_port_by_index()"

#: ../src/streamwidget.cc:52
#, fuzzy
msgid "Terminate"
msgstr "Finalitza la reproducció"

#: ../src/pavuapplication.cc:152
msgid "Select a specific tab on load."
msgstr ""

#: ../src/pavuapplication.cc:153
msgid "number"
msgstr ""

#: ../src/pavuapplication.cc:158
msgid "Retry forever if pa quits (every 5 seconds)."
msgstr ""

#: ../src/pavuapplication.cc:163
msgid "Maximize the window."
msgstr ""

#: ../src/pavuapplication.cc:168
msgid "Show version."
msgstr ""

#~ msgid "50%"
#~ msgstr "50%"

#~ msgid ""
#~ "All Input Devices\n"
#~ "All Except Monitors\n"
#~ "Hardware Input Devices\n"
#~ "Virtual Input Devices\n"
#~ "Monitors"
#~ msgstr ""
#~ "Tots els dispositius d'entrada\n"
#~ "Tots excepte els monitors\n"
#~ "Dispositius d'entrada físics\n"
#~ "Dispositius d'entrada virtuals\n"
#~ "Monitors"

#~ msgid ""
#~ "All Output Devices\n"
#~ "Hardware Output Devices\n"
#~ "Virtual Output Devices"
#~ msgstr ""
#~ "Tots els dispositius de sortida\n"
#~ "Dispositius de sortida físics\n"
#~ "Dispositius de sortida virtuals"

#~ msgid ""
#~ "All Streams\n"
#~ "Applications\n"
#~ "Virtual Streams"
#~ msgstr ""
#~ "Tots els fluxes\n"
#~ "Aplicacions\n"
#~ "Fluxes virtuals"

#~ msgid "<small>Max</small>"
#~ msgstr "<small>Max</small>"

#~ msgid "pa_context_move_sink_input_by_index() failed"
#~ msgstr "Ha fallat pa_context_move_sink_input_by_index()"

#~ msgid "pa_context_move_source_output_by_index() failed"
#~ msgstr "Ha fallat pa_context_move_source_output_by_index()"
